@extends('layouts.app')

@section('content')
  <form action="product" class="container" method="POST">
      @csrf
    <div class="mb-3">
        <label class="form-label">Email address</label>
        <input type="email" class="form-control"  id="email" aria-describedby="emailHelp">
      </div>
      <div class="mb-3">
        <label  class="form-label">Password</label>
        <input type="password" class="form-control" id="password">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
