@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['route' => 'product.store', 'autocomplete' => 'off','files'=>true]) !!}
        <div class="mb-3">
            {!! Form::label('user_id', 'User') !!}
            {!! Form::text('user_id', 1, ['class' => 'form-control','readonly']) !!}
        </div>
        <div class="mb-3">
            {!! Form::label('name', 'Título') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @error('name')
                <br>
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="mb-3">
            {!! Form::label('slug', 'Slug') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            @error('slug')
                <br>
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="row mb-3">
            <div class="col" style="position: relative; padding-bottom:56.25%">
                <img id="picture" style="position:absolute; object-fit:cover; width:50%; height:50%"
                    src="https://ngoclb.com/wp-content/uploads/2019/01/broken-1.png" alt="" />
            </div>
            <div class="col">
                <div class="form-group">
                    {!! Form::label('file', 'Imagen producto') !!}
                    {!! Form::file('file', ['class' => 'form-control-file','accept'=>"image/*"]) !!}
                </div>
                <div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Officiis nulla sequi</div>
            </div>
        </div>
        <div class="mb-3">
            {!! Form::label('autor', 'Autor') !!}
            {!! Form::text('autor', null, ['class' => 'form-control']) !!}
            @error('autor')
                <br>
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="mb-3">
            {!! Form::label('ISBN', 'ISBN') !!}
            {!! Form::text('ISBN', null, ['class' => 'form-control']) !!}
            @error('ISBN')
                <br>
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="dropdown">
            {!! Form::label('grupedit_id', 'Grupo editorial') !!}
            {!! Form::select('grupedit_id', $grupedit, null, ['class' => 'form-control']) !!}
            <br>
        </div>

        <div class="dropdown">
            {!! Form::label('category_id', 'Categoria') !!}
            {!! Form::select('category_id', $grupedit, null, ['class' => 'form-control']) !!}
            <br>
        </div>

        <div class="dropdown">
            {!! Form::label('edit_id', 'Editorial') !!}
            {!! Form::select('edit_id', $edit, null, ['class' => 'form-control']) !!}
            <br>
            @error('edit_id')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="mb-3">
            {!! Form::label('sinopsis', 'Sinopsis') !!}
            {!! Form::textarea('sinopsis', null, ['class' => 'form-control']) !!}
            {{-- @error('sinopsis')
                <small class="text-danger">{{ $message }}</small>
            @enderror --}}
        </div>
        <div class="mb-3">
            <p> Stock</p>
            <label>
                {!! Form::radio('stock1', 1, true) !!}
                Sí
            </label>
            <label>
                {!! Form::radio('stock1', 2, false) !!}
                No
            </label>
            <br>
            {{-- @error('stock1')
                <small class="text-danger">{{ $message }}</small>
            @enderror --}}
        </div>

        {!! Form::submit('Crear producto', ['class' => 'btn btn-primary']) !!}
        {{-- <a class="btn btn-primary" type="submit" href="{{route('product.index')}}">Crear</a> --}}

        {!! Form::close() !!}
    </div>
    @stop
    @section('js')
    <script>
        //Cambiar imagen
        document.getElementById("file").addEventListener('change', cambiarImagen);

        function cambiarImagen(event) {
            console.log('pasa')
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }

    </script>
@endsection
