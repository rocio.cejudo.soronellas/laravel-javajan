<?php

namespace Database\Seeders;

use App\Models\Grupedit;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prods = Product::factory(20)->create();

        foreach ($prods as $prod) {
            Image::factory(1)->create([
                'img_id' => $prod->id,
                'img_type' => Product::class
            ]);
        }
    }
}
