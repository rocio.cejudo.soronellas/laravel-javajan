<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //many to many tag
    use HasFactory;
    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
