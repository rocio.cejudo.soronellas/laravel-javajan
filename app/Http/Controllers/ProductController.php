<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Edit;
use App\Models\Grupedit;

use Illuminate\Support\Facades\Storage;

use App\Http\Requests\StoreProductRequest;
use App\Models\Category;

class ProductController extends Controller
{
    public function index(){
        $product=Product::where('stock1',1)->get();

        return view('product.index', compact('product'));
    }

    public function create(){
        $edit = Edit::pluck('name','id');
        $grupedit= Grupedit::pluck('name','id');
        $category= Category::pluck('name','id');
        return view('product.create', compact('grupedit','edit','category'));
    }

    public function store(Request $request){
        // return
      $product= Product::create($request->all());

      if($request->file('file')){
        $url= Storage::put('product', $request->file('file'));

        $product->image()->create([
            'url'=> $url
        ]);
      };

      if($request->grupedit){
        $product->edit()->attach($request->edit);
        $product->grupedit()->attach($request->grupedit);

      }
      return redirect()->route('product.index',$product);
    }
}
