@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Bienvenido de nuevo</h2>


        <div class="d-flex mb-3">
            <a class="btn btn-primary" style="margin-right: 10px;" type="button" href="{{route('product.create')}}">Crear</a>
            <a class="btn btn-primary" type="button" href="/login">Login</a>
        </div>

        <div class="row product">

            @foreach ($product as $key)
                <div class="card col-sm-3" style="width: 18rem;margin:0 20px 20px 20px;">
                    <img src="@if($key->image) {{ Storage::url($key->image->url) }} @else https://ngoclb.com/wp-content/uploads/2019/01/broken-1.png @endif" class="card-img-top" alt={{ $key->name }}
                        title={{ $key->name }}/>
                    <div class="card-body">
                        <h2 class="card-title">{{ $key->name }}</h2>
                        <h3>{{ $key->autor }}</h3>
                        <p class="card-text">{{ $key->sinopsis }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
