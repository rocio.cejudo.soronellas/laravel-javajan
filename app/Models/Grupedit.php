<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupedit extends Model
{
    use HasFactory;
    public function products(){
        // podemos acceder a todos los productos
        // que estan asignado a esta categoria
        return $this->hasMany(Product::class);
    }

    public function edits(){
        return $this->belongsTo(Edit::class);
    }
}
