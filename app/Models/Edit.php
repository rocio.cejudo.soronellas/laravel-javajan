<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Edit extends Model
{
    use HasFactory;

    public function grupedits(){
        // podemos acceder a todos los productos
        // que estan asignado a esta categoria
        return $this->hasMany(Grupedit::class);
    }
}
