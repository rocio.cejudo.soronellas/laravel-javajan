<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(){
        $product=Category::all()->get();

        return view('product.index', compact('product'));
    }

    public function create(){
        $product=Category::all()->get();

        return view('product.create', compact('product'));
    }
}
