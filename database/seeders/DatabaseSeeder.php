<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Edit;
use App\Models\Tag;
use App\Models\Grupedit;
use Illuminate\Database\Seeder;


use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('product');
        Storage::makeDirectory('product');

        $this->call(UserSeeder::class);
        Category::factory(4)->create();
        Edit::factory(3)->create();
        Tag::factory(8)->create();
        Grupedit::factory(3)->create();
        $this->call(ProductSeeder::class);
    }
}
