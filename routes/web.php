<?php

use App\Models\Product;
use App\Models\Category;

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserAuth;

Route::get('/login');

Route::post('user',[UserAuth::class,'userLogin']);

Route::view('login', 'login');

// Auth::routes();

Route::get('/',  [ProductController::class,'index'])->name('product.index');

Route::resource('product',ProductController::class);
