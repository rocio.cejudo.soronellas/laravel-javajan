<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Grupedit;
use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name= $this->faker->unique()->sentence();
        $autor= $this->faker->unique()->word(20);
        return [
            'name'=> $name,
            'slug'=> Str::slug($name),
            'autor'=> $autor,
            'sinopsis'=> $this->faker->text(250),
            'ISBN'=>$this->faker->text(9),
            'grupedit_id'=> Grupedit::all()->random()->id,
            'category_id'=> Category::all()->random()->id,
            'stock1'=>$this->faker->randomElement([1,2]),
            'user_id'=> User::all()->random()->id,
        ];
    }
}
