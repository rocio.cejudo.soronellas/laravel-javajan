@extends('layouts.app')

@section('content')
    <form action="" method="post">
        <h2>Creador de categoría</h2>
        <div class="mb-3">
            <label for="name" class="form-label">Nombre categoría</label>
            <input type="text" class="form-control" id="name">
        </div>
        <div class="mb-3">
            <label for="slug" class="form-label">Slug</label>
            <input type="text" class="form-control" id="slug">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
