<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
            'stock1' => 'required|in:1,2',
            'grupedit_id' => 'required',
            'edit' => 'required',
            'sinopsis' => 'required',
            'autor' => 'required'
        ];

        if ($this->stock1 === 2) {
            $rules = array_merge($rules, [
                'ISBN' => 'required'
            ]);
        }
        return $rules;
    }
}
