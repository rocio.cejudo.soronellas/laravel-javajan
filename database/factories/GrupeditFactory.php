<?php

namespace Database\Factories;
use App\Models\Grupedit;
use App\Models\Edit;
use Illuminate\Database\Eloquent\Factories\Factory;

use Illuminate\Support\Str;

class GrupeditFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Grupedit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $grupedit= $this->faker->unique()->sentence();

        return [
            'name'=> $grupedit,
            'slug'=> Str::slug($grupedit),
            'edit_id'=> Edit::all()->random()->id,
        ];
    }
}
