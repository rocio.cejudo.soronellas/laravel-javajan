<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded=['id','created_at','updated_at'];
    public function category(){
        return $this->belongsTo(Category::class);
    }

    //relación inversa usuarios
    public function user(){
        return $this->belongsTo(User::class);
    }

    //many to many tag
    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    public function grupedits(){
        return $this->belongsTo(Grupedit::class);
    }


    //one by one img
    public function image(){
        return $this->morphOne(Image::class,'img');
    }

}
